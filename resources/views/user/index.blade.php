@extends('layouts.app')

@section('content')
<div class="container py-5">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6 mx-auto">                               
                    <div class="card rounded-0">
                        <div class="card-header">
                            <h3 class="">Listado de usuarios</h3>
                            <a href="{{ route('user.create') }}" class="btn btn-sm btn-primary pull-right">
                                 Crear
                            </a>
                            <a href="{{ url('/') }}" class="btn btn-sm btn-primary pull-right">
                                Inicio
                            </a>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-hover">
                                <thead>
                                     Clic <a href="{{ route('user.excel') }}">aquí</a> para descargar excel
                                    <tr>
                                        <th width="10px">ID</th>
                                        <th>Nombre</th>
                                        <th>Cédula</th>
                                        <th colspan="3">&nbsp</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{ $user->id }}</td> 
                                            <td>{{ $user->name }}</td> 
                                            <td>{{ $user->identification }}</td> 
                                            <td width="10px">
                                             <a href="{{ route('user.show', $user->id) }}" class="btn btn-sm btn-default">
                                                Ver
                                             </a>
                                        </td>
                                            <td width="10px">
                                                <a href="{{ route('user.edit', $user->id) }}" class="btn btn-sm btn-default">
                                                    Editar
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>     
                    </div>
                </div>
            </div>
        </div>
           
    </div>
        
</div>
    
@endsection