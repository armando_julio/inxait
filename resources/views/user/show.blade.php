@extends('layouts.app')

@section('content')
<div class="container py-5">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6 mx-auto">
    
                    <div class="card rounded-0">
                        <div class="card-header">
                            <h3 class="">Listado de usuarios</h3>
                            <a href="{{ route('user.create') }}" class="btn btn-sm btn-primary pull-right">
                                 Crear
                            </a>
                            <a href="{{ route('user.index') }}" class="btn btn-sm btn-primary pull-right">
                                Volver
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="panel-body">
                                <p><strong>Id: </strong>{{ $user->id }}</p>
                                <p><strong>Nombre: </strong>{{ $user->name }}</p>
                                <p><strong>Slug: </strong>{{ $user->last_name }}</p>
                                <p><strong>Cédula: </strong>{{ $user->identification }}</p>
                                <p><strong>Celular: </strong>{{ $user->phone }}</p>
                                <p><strong>Correo: </strong>{{ $user->email }}</p>
                            </div>
                        </div>     
                    </div>
                </div>
            </div>
        </div>
           
    </div>
        
</div>
    
@endsection