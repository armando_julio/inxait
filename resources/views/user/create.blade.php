@extends('layouts.app')

@section('content')

<div class= "container py-5">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body">    
                <form action="{{ route('user.store') }}" method="POST">
                    {{ csrf_field() }} 
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nombre:</label>
                        <input type="text" class="form-control" id="name" pattern="{1,15}" placeholder="Nombre" maxlength="120" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Apellido:</label>
                        <input type="text" class="form-control" id="last_name" aria-describedby="emailHelp" placeholder="Apellido" maxlength="120" name="last_name" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Cédula:</label>
                        <input type="number" class="form-control" id="identification" aria-describedby="emailHelp" placeholder="Cédula" maxlength="120" name="identification" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Celular:</label>
                        <input type="number" class="form-control" id="phone" aria-describedby="emailHelp" placeholder="Celular" maxlength="120" name="phone" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Correo:</label>
                        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Correo" maxlength="120" name="email" required>
                    </div>
                    <div class="checkbox">
                        <label class="small">
                            <input name="terms" type="checkbox" required>Autorizo el tratamiento de mis datos de acuerdo con la
                            finalidad establecida en la política de protección de datos personales.<a href="#">Haga clic aquí</a>
                        </label>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <a href="{{ url('/') }}"> regresar</a>
                </form>
            </div>        
        </div>        
    </div>    
</div>

@endsection

@section('script')

<script type="text/javascript">
    var input = document.getElementById('name');
    var input = document.getElementById('last_name');
    var input = document.getElementById('identification');
    var input = document.getElementById('phone');
    var input = document.getElementById('email');

    input.oninvalid = function(event) {
        event.target.setCustomValidity('Campo requerido');
    }

</script>

@stop