<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\States;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::orderBy('id', 'DESC')->paginate();
        /*$usercount = User::count();
        
        if ($usercount == 5) {
            //dd("verdadero");
            $random_number= (random_int(1, 5));
        } else {
            dd("falso");
        }
        $userwiner = User::where('id', $random_number);*/

        return view('user.index', compact('users'));
    }

    public function create()
    {
        $stateses = States::orderBy('id', 'DESC')->paginate();

        return view('user.create', compact('stateses'));
    }

    public function store(Request $request)
    {
        $user = new User($request->all());
    
        $user->save();

        return redirect()->route('user.index')->with('info', 'Etiqueta creada con exito');
    }

    public function exportExcel()
    {
        return Excel::download(new UsersExport, 'user-list.xlsx');
    }

    public function show($id)
    {
        $user = User::find($id);

        return view('user.show', compact('user'));
    }

    public function edit($id)
    {
        $user = User::find($id);

        return view('user.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->fill($request->all())->save();

        return redirect()->route('user.index', $user->id)
            ->with('info', 'Etiqueta actualizada con exito');
    }

}
